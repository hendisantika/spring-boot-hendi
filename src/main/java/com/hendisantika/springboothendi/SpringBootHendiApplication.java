package com.hendisantika.springboothendi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHendiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHendiApplication.class, args);
	}
}
