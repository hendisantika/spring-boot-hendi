package com.hendisantika.springboothendi.domain;

import javax.persistence.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-hendi
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/10/17
 * Time: 21.58
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Table(name = "people")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "person_id")
    private long id;

    private String name;
    private int age;

    public Person() {
        // empty constuctor for Hibernate
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
