package com.hendisantika.springboothendi.repository;

import com.hendisantika.springboothendi.domain.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-hendi
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/10/17
 * Time: 22.04
 * To change this template use File | Settings | File Templates.
 */

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
    Collection<Person> findAll();

//    Person findByUsername(String username);
}
